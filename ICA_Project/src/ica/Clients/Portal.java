/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;

import ica.Util.Message;
import ica.Util.MetaAgent;
import ica.Util.SocketUtil;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author t7207089
 */
public class Portal extends MetaAgent {

    Socket Sock;
    String IP;
    int Port;
    SocketUtil SU;
    private final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    HashMap<String, Agent> joinedClients = new HashMap();
    String ID;

    public Portal(String IP, int Port) throws IOException {
        this.IP = IP;
        this.Port = Port;
        this.Sock = new Socket(IP, Port);
        SU = new SocketUtil(Sock);

        ID = CreateID();
     
        System.out.println("Portal Created With ID " + ID);
        Thread ListenThread = new Thread(() -> {
            ListenForMessage(SU);
        });
        ListenThread.start();
        Thread SendThread = new Thread(() -> {
            sendMessage();
        });
        SendThread.start();

    }

    public Portal() throws IOException {

        try {
            this.IP = "localhost";
            this.Port = 12346;
            this.Sock = new Socket(IP, Port);
            SU = new SocketUtil(Sock);

        } catch (IOException ex) {
            System.out.println("error");
        }
        this.Sock = new Socket(IP, Port);
        SU = new SocketUtil(Sock);
        ID = CreateID();
        System.out.println("Portal Created With ID " + ID);
        Thread ListenThread = new Thread(() -> {
            ListenForMessage(SU);
        });
        ListenThread.start();
        Thread SendThread = new Thread(() -> {
            sendMessage();
        });
        SendThread.start();

    }

    public void sendMessage() {

        while (true) {
            if (Que.isEmpty() != true) {
                Message M = this.Deque();
                System.out.println(M.getTo());
                if (M.Chk(getID())) {

                    System.out.println("Portal: " + getID() + "Recived the same message twice,Message Removed from que and disregarded");
                } else {

                    String a = M.getTo();
                    if (joinedClients.containsKey(a)) {
                        joinedClients.get(a).recieveMessage(M);
                    } else {
                        System.out.println("Send to Router from " + this.ID);
                        try {

                            M.AddBeen(this.getID());
                            SendtoRouter(M);
                        } catch (IOException ex) {
                            System.out.println("Error Sending to Server");
                        }
                    }
                }
            }
        }

    }

    private void SendtoRouter(Message m) throws IOException {
        SendMes(m);
    }

    public void attach(Agent a) {
        joinedClients.put(a.getId(), a);
    }

    private String CreateID() {
        String ID = "";
        for (int i = 0; i < 15; i++) {
            int number = (int) (Math.random() * 61 + 1);
            char ch = CHAR_LIST.charAt(number);
            ID = ID + ch;

        }
        return ID;
    }

    private void ListenForMessage(SocketUtil SU) {

        while (true) {
            SU.ReciveMsg(this);

        }
    }

    public String getID() {
        return this.ID;
    }

    public void SendMes(Message msg) throws IOException {
        SU.sMsg(msg);
    }

}
