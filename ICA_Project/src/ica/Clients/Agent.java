/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;

import ica.Util.Message;
import ica.Util.WeatherData;

/**
 *
 * @author t7066356
 */
public class Agent {

    Portal p;
    String id;

    public Agent(Portal portal, String i) {
        p = portal;
        this.id = i;

    }

    public void attachTo() {
        this.p.attach(this);
    }

    public void attachTo(Portal p) {
        p.attach(this);
    }

    public void sendMessege(Message m){
        p.EnQue(m);
        System.out.println("Message Enque");

    }

    public void recieveMessage(Message m) {
        System.out.println("Inside Agent");
        System.out.println("recieve message from " + m.getFrom());
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
         public Message CreateMsg(String Dest,WeatherData Wd) {
        Message m = new Message(getId(),Dest,Wd);
        return m;
    }


}
