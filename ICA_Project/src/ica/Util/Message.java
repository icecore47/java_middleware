/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Util;

import ica.Util.WeatherData;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author TB
 */
public class Message implements Serializable {


    private String from;
    private String to;
    private WeatherData content;
    private ArrayList Been = new ArrayList();
    
    //Constructor 
    public Message(String from, String to, WeatherData content) {
        this.from = from;
        this.to = to;
        this.content = content;
    }
    
    //Getters
    public String getFrom() { return from; }
    public String getTo() { return to; }
    public WeatherData getContent() { return content; }


    //Setters
    public void setFrom(String from) {this.from = from; }
    public void setTo(String to) { this.to = to;}
    public void setContent(WeatherData content) {this.content = content; }
    public boolean Chk(String i){
    return Been.contains(i);
    }
    
    public void AddBeen(String x){
    this.Been.add(x);
    }

    @Override
    public String toString() {
        return "Message{" + "from=" + from + ", to=" + to + ", content=" + content.toString() + ", Been=" + Been + '}';
    }
    
  
  
        
}
