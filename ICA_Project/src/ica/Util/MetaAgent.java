/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Util;

import java.util.concurrent.LinkedBlockingQueue;


/**
 *
 * @author TB
 */
public abstract class MetaAgent {
    
protected LinkedBlockingQueue Que = new LinkedBlockingQueue();


 public void EnQue(Message msg)  {
       try {
           Que.put(msg);
       } catch (InterruptedException ex) {
           System.out.println("Error Adding Message to Que");
       }
    }
    
 public Message Deque()  {
        Message msg = null;
       try {
           msg = (Message) Que.take();
       } catch (InterruptedException ex) {
          System.out.println("Error Removing Message to Que");
       }
       return msg;
    }
    
 public int getQueSuze(){
 return Que.size();
 }
    
}
