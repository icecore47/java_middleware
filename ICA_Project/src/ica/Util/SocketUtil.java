/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Util;


import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TB
 */
public class SocketUtil {

    private final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private final  Socket Sock;
    private  InputStream IS;
    private  ObjectInputStream OIS;
    private  OutputStream OS;
    private  ObjectOutputStream OOS;
    private final  String Socket_ID;
    private NodeMonitor NodeMon;  

    public SocketUtil(Socket sock) throws IOException {
        this.Sock = sock;        
        Socket_ID = CreateID();
        NodeMon = new NodeMonitor();

    }

    public void sMsg(Message Msg) {
        try {
            OS = this.Sock.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(SocketUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            OOS = new ObjectOutputStream(OS);
        } catch (IOException ex) {
            Logger.getLogger(SocketUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            OOS.writeObject(Msg);
              NodeMon.LogMsg(Msg,"Sent");
        } catch (IOException ex) {
            Logger.getLogger(SocketUtil.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(" Error in OOS");
        }
        
        System.out.println("Message Sent to " + Msg.getTo());
    }

    public void ReciveMsg(MetaAgent x){
         
                Thread R = new Thread(() -> {
         
        
        boolean loop = true;
        try {
            IS = this.Sock.getInputStream();
            OIS = new ObjectInputStream(IS);
                  
            Message tempMsg = null;
    
                while (loop) {
                    tempMsg = (Message) OIS.readObject();
                    
                    if (tempMsg != null) {
                        
                        x.EnQue(tempMsg);
                        NodeMon.LogMsg(tempMsg,"Recived");
                  
                    }
                }
            }   catch (ClassNotFoundException ex) { 
                  System.out.println("ClassNotFoundException");
                     //   Logger.getLogger(SocketUtil.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        System.out.println("IO Exception");
                      //  Logger.getLogger(SocketUtil.class.getName()).log(Level.SEVERE, null, ex);
                    } 
            

});
                R.start();
    }

       private String CreateID() {
        String ID = "";
        for (int i = 0; i < 15; i++) {
            int number = (int) (Math.random() * 61 + 1); 
            char ch = CHAR_LIST.charAt(number);
            ID = ID + ch;
           
        }
        return ID;
    }

    public String getID() {
        return this.Socket_ID;
    }

}
