/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Server;

import ica.Util.Message;
import ica.Util.MetaAgent;
import ica.Util.SocketUtil;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author t7066356
 */
public class Router extends MetaAgent {

    final int port = 12345;
      HashMap<String, SocketUtil> Portals = new HashMap();

  

    public void runServer(int port) throws IOException, InterruptedException {
      final ServerSocket SrvSock = new ServerSocket(port);
        System.out.println("Server Started");
         Thread LThread = new Thread(() -> {
          try {
              Listen(SrvSock);
          } catch (Exception ex) {
              System.out.println(" Error in Listen");
          } 
        });
         LThread.start();
         
    }

    private void Listen(ServerSocket SrvSock) throws IOException, InterruptedException {
        while (true) {

        Socket s = SrvSock.accept();
        SocketUtil sock = new SocketUtil(s);
        Portals.put(sock.getID(),sock);
        Thread RThread = new Thread(() -> {
        sock.ReciveMsg(this);
        });
        RThread.start();
      
        }
    }


    public Router() throws IOException, InterruptedException {
        this.runServer(port);

        Thread SThread = new Thread(() -> {
            try {
 
                CheckQue();

            } catch (InterruptedException ex) {
                System.out.println("Sync Error");
            }
        });
        SThread.start();

    }

    public void CheckQue() throws InterruptedException {
        
        while (true) {   
            synchronized (Que) {
                if (getQueSuze() != 0) {
                    SendFromQue();
                }
            }
        }
    }

    public void SendFromQue() throws InterruptedException {
   

        Message msg = (Message) this.Deque();
        for (SocketUtil sock : Portals.values()) {
            sock.sMsg(msg);
        }

    }

}
