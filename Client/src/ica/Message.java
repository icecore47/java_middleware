/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica;


/**
 *
 * @author TB
 */
public class Message {


    private String from;
    private String to;
    private int Hops;
    private WeatherData content;

    public int getHops() {
        return Hops;
    }

    public void setHops(int Hops) {
        this.Hops = Hops;
    }
   
    
    //Constructor 
    public Message(String from, String to,int Hops, WeatherData content) {
        this.from = from;
        this.to = to;
        this.Hops = Hops;
        this.content = content;
    }
        public Message(String from, String to, WeatherData content) {
        this.from = from;
        this.to = to;
        this.Hops = 30;
        this.content = content;
    }
    
    //Getters
    public String getFrom() { return from; }
    public String getTo() { return to; }
    public WeatherData getContent() { return content; }


    //Setters
    public void setFrom(String from) {this.from = from; }
    public void setTo(String to) { this.to = to;}
    public void setContent(WeatherData content) {this.content = content; }
    public static Message Parse(String in) {
        
        String tempFrom = in.substring(in.indexOf('=')+1,in.indexOf(", "));
        in = in.substring(in.indexOf(tempFrom)+tempFrom.length()+2);
        String tempTo =in.substring(in.indexOf('=')+1,in.indexOf(", "));
        in = in.substring(in.indexOf(tempFrom)+tempTo.length()+6);
        String temp =in.substring(in.indexOf('=')+1,in.indexOf(", "));
        int tempHop = Integer.parseInt(temp);
        in = in.substring(in.indexOf(temp)+temp.length()+6);
        String a = in.substring(in.indexOf("WindSpeed=")+10,in.indexOf(", "));
        int tempWindSpeed = Integer.parseInt(a);
        in = in.substring(in.indexOf(a)+a.length()+2);
        String b = in.substring(in.indexOf('=')+1,in.indexOf(","));
        int tempTemp = Integer.parseInt(b);
        in = in.substring(in.indexOf(b)+b.length()+2);
        String c = in.substring(in.indexOf('=')+1,in.indexOf("}"));
        int tempHumidity = Integer.parseInt(c);
        
        Message msg = new Message(tempFrom,tempTo,tempHop, new WeatherData(tempWindSpeed,tempTemp,tempHumidity));
        return msg;
        
    }

    @Override
    public String toString() {
        return "Message{" + "from=" + from + ", to=" + to + ", Hops=" + Hops + ", content=" + content + '}';
    }

      
    
}
