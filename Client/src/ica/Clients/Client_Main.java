/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;

import ica.Message;
import ica.WeatherData;
import java.io.IOException;

/**
 *
 * @author t7066356
 */
public class Client_Main {
    
   
    public static void main(String[] args) throws IOException {

        WeatherData w = new WeatherData(1, 2, 3);
        System.out.println("Weather Data Created");
        System.out.println("Message Created");
        Portal p1 = new Portal();
        System.out.println("Portal Created");

        Agent a1 = new Agent(p1, "SensorStation");
        System.out.println("Agent1 Created");

        Agent a2 = new Agent(p1, "PowerStation");
        System.out.println("Agent2 Created");

        a1.attachTo();
        System.out.println("Agent1 attached to portal");

        a2.attachTo();
        System.out.println("Agent2 attached to portal");

        try {
            
            a1.sendMessege(a1.CreateMsg("PowerStation", w));
            System.out.println("Message Sent in Main");

        } catch (InterruptedException ex) {
            System.out.println("Error");
        }

    }
    
    
    
}
