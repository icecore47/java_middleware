/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;


import ica.Message;
import ica.SocketUtil;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author t7066356
 */
public class ClientThread extends Thread{
    
    
    Socket Sock;
    Client c;

    public ClientThread(Socket Sock,Client c) {
        this.Sock = Sock;
        this.c = c;
    }
    
    @Override
    public void run(){
        System.out.println("Inside Run");
       SocketUtil sock = new SocketUtil(Sock);
        Message msg = sock.ReciveMsg();
        try {
            c.messagesQueue.put(msg);
        } catch (InterruptedException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
               
    }
    
}
