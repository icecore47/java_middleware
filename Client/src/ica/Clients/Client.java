/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;

import ica.Message;
import ica.SocketUtil;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import ica.SocketUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author t7066356
 */
public class Client {

  final LinkedBlockingQueue messagesQueue = new LinkedBlockingQueue();
    Socket Sock;
    String IP;
    int Port;
    SocketUtil x;
    
    
    
    // This Constructor Allows you Connect to a Specfic Router
    public Client(String IP, int Port) throws IOException {
        this.IP = IP;
        this.Port = Port;
        this.Sock = new Socket(IP,Port);
        this.x = new SocketUtil(Sock);
         new ClientThread(Sock,this).start();
    }
    
    // This one Connects to Hard Coded Router... In this Example Localhost
        public Client(){
        this.IP = "localhost";
        this.Port = 12346;
      try {
          this.Sock = new Socket(IP,Port);
      } catch (IOException ex) {
          System.out.println("ERROR - Server Can Not Accept Connection");
          System.exit(0);
      }
        this.x = new SocketUtil(Sock);
         new ClientThread(Sock,this).start();
    }
        
        
        // Sends Message
      public void SendMes(Message msg) {
        System.out.println("here");
        this.x.sMsg(msg);
        System.out.println("Message Sent");
    }

        
}
