/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Clients;

import ica.Message;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author t7207089
 */
public class Portal extends Client {

    HashMap<String, Agent> joinedClients = new HashMap();

    public Portal(String IP, int Port) throws IOException {
        super(IP, Port);
        ListenForMessage();
    }

    public Portal() throws IOException {
        super();
        ListenForMessage();
    }

    public void enque(Message message) throws InterruptedException {
        synchronized (messagesQueue) {
            messagesQueue.put(message);
            System.out.println("Message in que");
            sendMessage();
        }
    }

    public void sendMessage() throws InterruptedException {
        Thread SendMsg = new Thread(() -> {

            System.out.println("Inside Send Message");
            while (messagesQueue.isEmpty() != true) {
                Message message;
                try {
                    message = (Message) messagesQueue.take();
                    String a = message.getTo();

                   if (joinedClients.containsKey(a)) {
                       
                      joinedClients.get(a).recieveMessage(message);
                    } else {
                        SendtoRouter(message);
                        // messages can be stuck in infite loop round the system due to this.
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Portal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        SendMsg.start();
    }

    private void SendtoRouter(Message m) {
        SendMes(m);
    }

    public void attach(Agent a) {
        joinedClients.put(a.getId(), a);
    }

    private void ListenForMessage() {
        Thread ListenThread = new Thread(() -> {
            while (true) {

                Message msg = this.x.ReciveMsg();
                if (msg != null) {
                    try {
                        messagesQueue.put(msg);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Portal.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            }

        });
        ListenThread.start();
    }
}
