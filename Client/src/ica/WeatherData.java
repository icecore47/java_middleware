/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica;

/**
 *
 * @author TB
 */
public class WeatherData {
    
   int WindSpeed;
   int Temp;
   int Humidity;



    public WeatherData(int WindSpeed, int Temp, int Humidity) {
        this.WindSpeed = WindSpeed;
        this.Temp = Temp;
        this.Humidity = Humidity;
    }
  ;

    public int getWindSpeed() {
        return WindSpeed;
    }

    public void setWindSpeed(int WindSpeed) {
        this.WindSpeed = WindSpeed;
    }

    public int getTemp() {
        return Temp;
    }

    public void setTemp(int Temp) {
        this.Temp = Temp;
    }

    public int getHumidity() {
        return Humidity;
    }

    public void setHumidity(int Humidity) {
        this.Humidity = Humidity;
    }
    
    @Override
    public String toString() {
        return "WeatherData{" + "WindSpeed=" + WindSpeed + ", Temp=" + Temp + ", Humidity=" + Humidity + '}';
    }
   
    
}
