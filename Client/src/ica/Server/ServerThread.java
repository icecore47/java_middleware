/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Server;


import ica.Message;
import ica.SocketUtil;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author t7066356
 */
public class ServerThread extends Thread{
    
    
    Socket Sock;
    Server s;

    public ServerThread(Socket Sock,Server s) {
        this.Sock = Sock;
        this.s = s;
    }
    
    @Override
    public void run(){
        System.out.println("Inside Run");
       SocketUtil sock = new SocketUtil(Sock);
        Message msg = sock.ReciveMsg();
       
        System.out.println("msg Recived From "+msg.getFrom());
        s.Portals.put(msg.getFrom(), sock);
        try {
            s.x.put(msg);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
