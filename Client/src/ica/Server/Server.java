/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica.Server;

import ica.SocketUtil;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author t7066356
 */
public abstract class Server {
    
    Map<String,SocketUtil> Portals  = new HashMap();
    LinkedBlockingQueue x;
  

    public void runServer(ServerSocket SrvSock,int port,LinkedBlockingQueue que) throws IOException {
        SrvSock = new ServerSocket(port);
        x = que;
        System.out.println("Server Started");
        Listen(SrvSock);
    }

    private void Listen(ServerSocket SrvSock) throws IOException {
        while (true) {
            Socket Server = SrvSock.accept();
            new ServerThread(Server,this).start();
        }
    }

   


}
