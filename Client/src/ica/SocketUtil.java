/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;

/**
 *
 * @author TB
 */
public class SocketUtil {

    private String ID;
    private final Socket Sock;
    private InputStreamReader Isr;
    private BufferedReader Br;
    private PrintWriter PW;




    public SocketUtil(Socket sock) {
        this.Sock = sock;
    }

    public String getID() {
        //setID();
        return this.ID;
    }

    public void sMsg(Message Msg) {
        Thread SendMsg = new Thread(() -> {
            SendMsg(Msg);
        });
        SendMsg.start();
    }

    public Message ReciveMsg() {
        Message msg = null;
        try {
            Isr = new InputStreamReader(Sock.getInputStream());
            Br = new BufferedReader(Isr);
            String Temp = Br.readLine();
            if (Temp != null) {
                System.out.println(Temp);
                msg = Message.Parse(Temp);
                msg.setHops(msg.getHops()-1);
                if (msg.getHops()==0){
                msg = null;
                }
                return msg;
            }

        } catch (IOException e) {
        }
        return msg;
    }

    private void SendMsg(Message Msg) {
        try {
            PW = new PrintWriter(Sock.getOutputStream(), true);
            String msg = Msg.toString();
            PW.println(msg);
        } catch (IOException e) {
        }
    }

}
